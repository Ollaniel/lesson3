package by.trainJava.lesson3;

public class six {
	public static void main(String[] args) {
		int n =6;
		int [][] f = new int[n][n];
		for (int i =0 ; i<n; i++) {
			f[i]= new int [n];
			for (int j =0 ; j<n; j++) {
				if ((i==0) || (i==n-1) || (j==0) || (j==n-1)) { 
						f[i][j]=1;
					} else {
						f[i][j]=0;
					}
				}
		}

		for (int i =0 ; i<n; i++) {
			System.out.println();
			for (int j =0 ; j<n; j++) {
				System.out.print(f[i][j]+" ");
			}
		}
		
	}
}
